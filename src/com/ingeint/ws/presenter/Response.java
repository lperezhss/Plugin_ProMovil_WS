package com.ingeint.ws.presenter;

public class Response {
	int id;
	boolean status;
	String message;
    Object json;
	
	public Response(boolean status, int id, String message, Object json) {
		super();
		this.status = status;
		this.id = id;
		this.message = message;
		this.json = json;
	}

	public boolean getStatus() {
		return status;
	}
	
	public void setStatus(boolean status) {
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	public Object getJson() {
		return json;
	}

	public void setJson(Object json) {
		this.json = json;
	}
}
