package com.ingeint.ws.presenter;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TipoIdentificacion {
    int id;
    String name;
    Boolean isdetailednames;
    private String coupon;

    @JsonProperty("id")
	public int getID() {
		return id;
	}
	
	public void setID(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

    @JsonProperty("isdetailednames")
	public Boolean getIsDetailedNames() {
		return isdetailednames;
	}
	
	public void setIsDetailedNames(Boolean isdetailednames) {
		this.isdetailednames = isdetailednames;
	}

	public String getCoupon() {
		return coupon;
	}

	public void setCoupon(String coupon) {
		this.coupon = coupon;
	}
}
