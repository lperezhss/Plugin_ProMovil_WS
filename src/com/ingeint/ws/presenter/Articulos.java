package com.ingeint.ws.presenter;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Articulos {
    private int id;
    private String value;
    private String name;
    private String ref;
    private double stock;
    private String type;
    private String taxtype;
    private String category_id;
    private double tax;
    private double price1;
    private double price2;
    private double price3;
    private double price4;
    private double price5;
    private String uom1;
    private String uom2;
    private double equiuom1;
    private double equiuom2;
    private String iscolorsize;
    private String coupon;
    
    @JsonProperty("id")
    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getStock() {
        return stock;
    }

    public void setStock(double stock) {
        this.stock = stock;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("taxtype")
    public String getTaxType() {
        return taxtype;
    }

    public void setTaxType(String taxtype) {
        this.taxtype = taxtype;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public double getPrice1() {
        return price1;
    }

    public void setPrice1(double price1) {
        this.price1 = price1;
    }

    public double getPrice2() {
        return price2;
    }

    public void setPrice2(double price2) {
        this.price2 = price2;
    }

    public double getPrice3() {
        return price3;
    }

    public void setPrice3(double price3) {
        this.price3 = price3;
    }

    public double getPrice4() {
        return price4;
    }

    public void setPrice4(double price4) {
        this.price4 = price4;
    }

    public double getPrice5() {
        return price5;
    }

    public void setPrice5(double price5) {
        this.price5 = price5;
    }

    @JsonProperty("uom1")
    public String getUOM1() {
        return uom1;
    }

    public void setUOM1(String uom1) {
        this.uom1 = uom1;
    }

    @JsonProperty("uom2")
    public String getUOM2() {
        return uom2;
    }

    public void setUOM2(String uom2) {
        this.uom2 = uom2;
    }

    @JsonProperty("equiuom1")
    public double getEquiUOM1() {
        return equiuom1;
    }

    public void setEquiUOM1(double equiuom1) {
        this.equiuom1 = equiuom1;
    }

    @JsonProperty("equiuom2")
    public double getEquiUOM2() {
        return equiuom2;
    }

    public void setEquiUOM2(double equiuom2) {
        this.equiuom2 = equiuom2;
    }

    @JsonProperty("category_id")
    public String getCategory_ID() {
        return category_id;
    }

    public void setCategory_ID(String category_id) {
        this.category_id = category_id;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    @JsonProperty("iscolorsize")
    public String getIsColorSize() {
        return iscolorsize;
    }

    public void setIsColorSize(String iscolorsize) {
        this.iscolorsize = iscolorsize;
    }

	public String getCoupon() {
		return coupon;
	}

	public void setCoupon(String coupon) {
		this.coupon = coupon;
	}

}
