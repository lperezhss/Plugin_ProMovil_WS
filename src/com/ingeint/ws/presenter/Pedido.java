package com.ingeint.ws.presenter;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Pedido {
    int id;
    int documentno = 0;
    int client_id;
    String datetrx;
    String dateto;
    String payment_condition;
    String name;
    int partner_id;
    int salesrep_id;
    String status = "0";
    List<PedidoReng> lines;
    
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	@JsonProperty("client_id")
    public int getClient_ID() {
		return client_id;
	}

	public void setClient_ID(int client_id) {
		this.client_id = client_id;
	}

	@JsonProperty("documentno")
	public int getDocumentNo() {
		return documentno;
	}
	
	public void setDocumentNo(int documentno) {
		this.documentno = documentno;
	}

    @JsonProperty("datetrx")
	public String getDateTrx() {
		return datetrx;
	}
	
	public void setDateTrx(String datetrx) {
		this.datetrx = datetrx;
	}

    @JsonProperty("dateto")
	public String getDateTo() {
		return dateto;
	}
	
	public void setDateTo(String dateto) {
		this.dateto = dateto;
	}

    @JsonProperty("payment_condition")
	public String getPaymentCondition() {
		return payment_condition;
	}
	
	public void setPaymentCondition(String payment_condition) {
		this.payment_condition = payment_condition;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

    @JsonProperty("partner_id")
	public int getPartner_ID() {
		return partner_id;
	}
	
	public void setPartner_ID(int partner_id) {
		this.partner_id = partner_id;
	}

    @JsonProperty("salesrep_id")
	public int getSalesRep_ID() {
		return salesrep_id;
	}
	
	public void setSalesRep_ID(int salesrep_id) {
		this.salesrep_id = salesrep_id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<PedidoReng> getLines() {
		return lines;
	}

	public void setLines(List<PedidoReng> lines) {
		this.lines = lines;
	}
}
