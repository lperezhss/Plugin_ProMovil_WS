package com.ingeint.ws.presenter;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author lperez
 *
 */
public class Clientes {
    private int id;
    private String value;
    private String name;
    private String taxid;
    private String address;
    private String phone;
    private String zone;
    private String email;
    private String tprice;
    private String salesrep_id;
    private String route;
    private String isdetailednames;
    private int taxidtype_id;
    private String firstname1;
    private String firstname2;
    private String lastname1;
    private String lastname2;
    private String coupon;

    @JsonProperty("id")
    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("taxid")
    public String getTaxID() {
        return taxid;
    }

    public void setTaxID(String taxid) {
        this.taxid = taxid;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("tprice")
    public String getTPrice() {
        return tprice;
    }

    public void setTPrice(String tprice) {
        this.tprice = tprice;
    }

    @JsonProperty("salesrep_id")
    public String getSalesRep_ID() {
        return salesrep_id;
    }

    public void setSalesRep_ID(String salesrep_id) {
        this.salesrep_id = salesrep_id;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    @JsonProperty("isdetailednames")
    public String getIsDetailedNames() {
        return isdetailednames;
    }

    public void setIsDetailedNames(String isdetailednames) {
        this.isdetailednames = isdetailednames;
    }

    @JsonProperty("taxidtype_id")
    public int getTaxIdType_ID() {
        return taxidtype_id;
    }

    public void setTaxIdType_ID(int taxidtype_id) {
        this.taxidtype_id = taxidtype_id;
    }

    @JsonProperty("firstname1")
    public String getFirstName1() {
        return firstname1;
    }

    public void setFirstName1(String firstName1) {
        this.firstname1 = firstName1;
    }

    @JsonProperty("firstname2")
    public String getFirstName2() {
        return firstname2;
    }

    public void setFirstName2(String firstName2) {
        this.firstname2 = firstName2;
    }

    @JsonProperty("lastname1")
    public String getLastName1() {
        return lastname1;
    }

    public void setLastName1(String lastName1) {
        this.lastname1 = lastName1;
    }

    @JsonProperty("lastname2")
    public String getLastName2() {
        return lastname2;
    }

    public void setLastName2(String lastName2) {
        this.lastname2 = lastName2;
    }

	public String getCoupon() {
		return coupon;
	}

	public void setCoupon(String coupon) {
		this.coupon = coupon;
	}
    
}
