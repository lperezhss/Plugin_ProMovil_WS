package com.ingeint.ws.presenter;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Cupon {
    String coupon;
    String company;
    String url = "";
    String user = "";
    String password = "";
    String company_id;
    String salesrep_id;
    String view_condition;
    String is_multi_uom;
    
	public String getCoupon() {
		return coupon;
	}
	
	public void setCoupon(String coupon) {
		this.coupon = coupon;
	}
	
	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getUser() {
		return user;
	}
	
	public void setUser(String user) {
		this.user = user;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

    @JsonProperty("company_id")
	public String getCompany_ID() {
		return company_id;
	}
	
	public void setCompany_ID(String company_id) {
		this.company_id = company_id;
	}

    @JsonProperty("salesrep_id")
	public String getSalesRep_ID() {
		return salesrep_id;
	}
	
	public void setSalesRep_ID(String salesrep_id) {
		this.salesrep_id = salesrep_id;
	}

    @JsonProperty("view_condition")
	public String getViewCondition() {
		return view_condition;
	}

	public void setViewCondition(String view_condition) {
		this.view_condition = view_condition;
	}

    @JsonProperty("is_multi_uom")
	public String getIsMultiUOM() {
		return is_multi_uom;
	}

	public void setIsMultiUOM(String is_multi_uom) {
		this.is_multi_uom = is_multi_uom;
	}

}
