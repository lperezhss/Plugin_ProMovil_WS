package com.ingeint.ws.presenter;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DocumCC {
    private int id;
    private String taxid;
    private String doctype;
    private String docnum;
    private String datetrx;
    private double amt;
    private double balance;
    private Boolean seleccion = false;
    private String coupon;

    @JsonProperty("id")
    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    @JsonProperty("taxid")
    public String getTaxID() {
        return taxid;
    }

    public void setTaxID(String taxid) {
        this.taxid = taxid;
    }

    @JsonProperty("doctype")
    public String getDocType() {
        return doctype;
    }

    public void setDocType(String doctype) {
        this.doctype = doctype;
    }

    @JsonProperty("docnum")
    public String getDocNum() {
        return docnum;
    }

    public void setDocNum(String docnum) {
        this.docnum = docnum;
    }

    @JsonProperty("datetrx")
    public String getDateTrx() {
        return datetrx;
    }

    public void setDateTrx(String datetrx) {
        this.datetrx = datetrx;
    }

    @JsonProperty("amt")
    public double getAmt() {
        return amt;
    }

    public void setAmt(double amt) {
        this.amt = amt;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Boolean getSeleccion() {
        return seleccion;
    }

    public void setSeleccion(Boolean seleccion) {
        this.seleccion = seleccion;
    }

	public String getCoupon() {
		return coupon;
	}

	public void setCoupon(String coupon) {
		this.coupon = coupon;
	}

}
