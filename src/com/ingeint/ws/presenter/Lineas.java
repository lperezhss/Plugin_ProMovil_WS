package com.ingeint.ws.presenter;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Lineas {
    private int id;
    private String value;
    private String name;
    private String coupon;
    
    @JsonProperty("id")
    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public String getCoupon() {
		return coupon;
	}

	public void setCoupon(String coupon) {
		this.coupon = coupon;
	}

}
