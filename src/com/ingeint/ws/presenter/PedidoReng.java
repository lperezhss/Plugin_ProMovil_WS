package com.ingeint.ws.presenter;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PedidoReng {
    int id;
    int line;
    int product_id;
    double qty = 0;
    double price = 0;
    
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

    @JsonProperty("line")
	public int getLine() {
		return line;
	}
	
	public void setLine(int line) {
		this.line = line;
	}

    @JsonProperty("product_id")
	public int getProduct_ID() {
		return product_id;
	}
	
	public void setProduct_ID(int product_id) {
		this.product_id = product_id;
	}
	
	public double getQty() {
		return qty;
	}
	
	public void setQty(double qty) {
		this.qty = qty;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}

}
