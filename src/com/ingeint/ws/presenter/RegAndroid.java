package com.ingeint.ws.presenter;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RegAndroid {
	String value;
	String name;
	int salesrep_id;
	String status;
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

    @JsonProperty("salesrep_id")
	public int getSalesRep_ID() {
		return salesrep_id;
	}
	
	public void setSalesRep_ID(int salesrep_id) {
		this.salesrep_id = salesrep_id;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
}
