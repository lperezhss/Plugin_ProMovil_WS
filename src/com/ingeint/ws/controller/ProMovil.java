package com.ingeint.ws.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.ingeint.ws.presenter.Pedido;
import com.ingeint.ws.presenter.Response;
import com.ingeint.ws.service.SArticulos;
import com.ingeint.ws.service.SClientes;
import com.ingeint.ws.service.SDocumCC;
import com.ingeint.ws.service.SOrder;
import com.ingeint.ws.service.SProMovil;

@Path("/promovil")
@Produces(MediaType.APPLICATION_JSON)
public class ProMovil {
	
	@GET
    @Path("/")
	public String post() {
		return "Prueba Exitosa ProMovil";
	}
	
	@GET
	@Path("/VerCupon")
	public Response getVerCupon(@PathParam("clientID") int clientID) {
		return SProMovil.getVerCupon();
	}
	
	@GET
	@Path("/ValidarEquipo/{clientID}/{androidID}/{salesRepID}")
	public Response getValidarEquipo(@PathParam("clientID") int clientID, @PathParam("androidID") String androidID, @PathParam("salesRepID") int salesRepID) {
		return SProMovil.getValidarEquipo(clientID, androidID, salesRepID);
	}
	
	@GET
	@Path("/VerTipoIdentificacion/{clientID}")
	public Response getVerTipoIdentificacion(@PathParam("clientID") int clientID) {
		return SClientes.getVerTipoIdentificacion(clientID);
	}
	
	@GET
	@Path("/VerClientes/{clientID}/{salesRepID}")
	public Response getVerClientes(@PathParam("clientID") int clientID, @PathParam("salesRepID") int salesRepID) {
		return SClientes.getVerClientes(clientID, salesRepID);
	}
	
	@GET
	@Path("/VerDocumentos/{clientID}/{salesRepID}")
	public Response getVerDocumentos(@PathParam("clientID") int clientID, @PathParam("salesRepID") int salesRepID) {
		return SDocumCC.getVerDocumentos(clientID, salesRepID);
	}
	
	@GET
	@Path("/VerArticulos/{clientID}")
	public Response getVerArticulos(@PathParam("clientID") int clientID) {
		return SArticulos.getVerArticulos(clientID);
	}
	
	@GET
	@Path("/VerLineas/{clientID}")
	public Response getVerLineas(@PathParam("clientID") int clientID) {
		return SArticulos.getVerLineas(clientID);
	}
	
	@GET
	@Path("/VerColores/{clientID}")
	public Response getVerColores(@PathParam("clientID") int clientID) {
		return SArticulos.getVerColores(clientID);
	}
	
	@GET
	@Path("/VerTallas/{clientID}")
	public Response getVerTallas(@PathParam("clientID") int clientID) {
		return SArticulos.getVerTallas(clientID);
	}
	
	@POST
	@Path("/GuardarPedido")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response GuardarPedido(Pedido pedido) {
		return SOrder.createPedido(pedido);
	}
}
