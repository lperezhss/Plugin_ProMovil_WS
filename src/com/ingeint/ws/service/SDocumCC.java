package com.ingeint.ws.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.compiere.util.DB;

import com.ingeint.ws.base.RequestEnv;
import com.ingeint.ws.presenter.Articulos;
import com.ingeint.ws.presenter.DocumCC;
import com.ingeint.ws.presenter.Response;

public class SDocumCC {
	
	public static Response getVerDocumentos(int clientID, int salesRepID) {
		String trxName = RequestEnv.getCurrentTrxName();
		Properties ctx = RequestEnv.getCtx();
		
		try {
			String sql = ""
					+ "SELECT c_bpartner_id id, \n"
					+ "    taxid, \n"
					+ "    DocTypeName doctype, \n"
					+ "    DocumentNo docnum, \n"
					+ "    DateInvoiced datetrx, \n"
					+ "    GrandTotal amt, \n"
					+ "    OpenAmt balance, \n"
					+ "    coupon \n"
					+ "FROM HSS_VerDocumentos \n"
					+ "where ad_client_id = ? \n"
					+ "	and hss_verdocumentos_id = ? \n";
			
			PreparedStatement pstmt = (PreparedStatement) DB.prepareStatement(sql, trxName);
			pstmt.setInt(1, clientID);
			pstmt.setInt(2, salesRepID);
			ResultSet rs = pstmt.executeQuery();
			
			List<DocumCC> documCCs = new ArrayList<DocumCC>();
			
			while (rs.next()) {
				DocumCC documCC = new DocumCC();
				documCC.setID(rs.getInt("id"));
				documCC.setTaxID(rs.getString("taxid"));
				documCC.setDocType(rs.getString("doctype"));
				documCC.setDocNum(rs.getString("docnum"));
				documCC.setDateTrx(rs.getString("datetrx"));
				documCC.setAmt(rs.getDouble("amt"));
				documCC.setBalance(rs.getDouble("balance"));
				documCC.setCoupon(rs.getString("coupon"));
				
				documCCs.add(documCC);
			}
			
			return new Response(true, 0, "", documCCs);
		} catch (Exception e) {
			return new Response(false, -1, "Error " + e.getMessage(), "");
		}
	}

}
