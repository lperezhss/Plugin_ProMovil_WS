package com.ingeint.ws.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.compiere.model.Query;
import org.compiere.util.DB;

import com.hss.model.MRegAndroid;
import com.ingeint.ws.base.RequestEnv;
import com.ingeint.ws.presenter.Cupon;
import com.ingeint.ws.presenter.RegAndroid;
import com.ingeint.ws.presenter.Response;

public class SProMovil {

	public static Response getVerCupon() {
		String trxName = RequestEnv.getCurrentTrxName();
		// Properties ctx = RequestEnv.getCtx();
		
		try {
			String sql = ""
					+ "select value coupon, \n"
					+ "	name company, \n"
					+ "	url, \n"
					+ "	'promovil' as user, \n"
					+ "	'promovil' as password, \n"
					+ "	hss_client company_id, \n"
					+ "	hss_salesrep salesrep_id, \n"
					+ "	hss_viewcondition view_condition, \n"
					+ "	hss_ismultiuom is_multi_uom \n"
					+ "from HSS_Cupon \n";
			
			PreparedStatement pstmt = (PreparedStatement) DB.prepareStatement(sql, trxName);
			ResultSet rs = pstmt.executeQuery();
			
			List<Cupon> cupons = new ArrayList<Cupon>();
			
			while (rs.next()) {
				Cupon cupon = new Cupon();
				cupon.setCoupon(rs.getString("coupon"));
				cupon.setCompany(rs.getString("company"));
				cupon.setUrl(rs.getString("url"));
				cupon.setUser(rs.getString("user"));
				cupon.setPassword(rs.getString("password"));
				cupon.setCompany_ID(rs.getString("company_id"));
				cupon.setSalesRep_ID(rs.getString("salesrep_id"));
				cupon.setViewCondition(rs.getString("view_condition"));
				cupon.setIsMultiUOM(rs.getString("is_multi_uom"));
				
				cupons.add(cupon);
			}
			
			return new Response(true, 0, "", cupons);
		} catch (Exception e) {
			return new Response(false, -1, "Error " + e.getMessage(), "");
		}
	}

	public static Response getValidarEquipo(int clientID, String androidID, int salesRepID) {
		String trxName = RequestEnv.getCurrentTrxName();
		Properties ctx = RequestEnv.getCtx();
		
		try {
			String sqlWhere = "AD_Client_ID = ? AND Value = ?";
			
			MRegAndroid regAndroid = new Query(ctx, MRegAndroid.Table_Name , sqlWhere, trxName)
					.setOnlyActiveRecords(true)
					.setParameters(new Object[] {clientID, androidID})
					.setOrderBy(MRegAndroid.COLUMNNAME_HSS_RegAndroid_ID)
					.first();
			
			if (regAndroid == null) {
				regAndroid = new MRegAndroid(ctx, 0, trxName);
				regAndroid.set_ValueOfColumn("AD_Client_ID", clientID);
				regAndroid.setAD_Org_ID(0);
				regAndroid.setValue(androidID);
				regAndroid.setName(androidID);
				regAndroid.setSalesRep_ID(salesRepID);
				regAndroid.setHSS_Status(MRegAndroid.HSS_STATUS_Certificado);
				regAndroid.saveEx();
			} else {
				if(regAndroid.getHSS_Status().equals(MRegAndroid.HSS_STATUS_Pendiente)) {
					if (com.hss.util.TimestampUtil.between(regAndroid.getCreated()) > 15) {
						regAndroid.setHSS_Status(MRegAndroid.HSS_STATUS_Vencido);
						regAndroid.saveEx();
					}
				}
			}
			
			RegAndroid regAndroid2 = new RegAndroid();
			regAndroid2.setValue(regAndroid.getValue());
			regAndroid2.setName(regAndroid.getName());
			regAndroid2.setSalesRep_ID(regAndroid.getSalesRep_ID());
			regAndroid2.setStatus(regAndroid.getHSS_Status());
			
			List<RegAndroid> rengAndroids = new ArrayList<RegAndroid>();
			rengAndroids.add(regAndroid2);
			
			return new Response(true, 0, "", rengAndroids);
		} catch (Exception e) {
			return new Response(false, -1, "Error " + e.getMessage(), "");
		}
	}
}
