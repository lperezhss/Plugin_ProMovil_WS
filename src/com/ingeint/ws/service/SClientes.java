package com.ingeint.ws.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.compiere.util.DB;

import com.ingeint.ws.base.RequestEnv;
import com.ingeint.ws.presenter.Clientes;
import com.ingeint.ws.presenter.Response;
import com.ingeint.ws.presenter.TipoIdentificacion;

public class SClientes {

	public static Response getVerClientes(int clientID, int salesRepID) {
		String trxName = RequestEnv.getCurrentTrxName();
		// Properties ctx = RequestEnv.getCtx();
		
		try {
			String sql = ""
					+ "select c_bpartner_id as id, \n"
					+ "    value, \n"
					+ "    name, \n"
					+ "    taxid, \n"
					+ "    coalesce(Address1, '') as address, \n"
					+ "    coalesce(Phone, '') as phone, \n"
					+ "    coalesce(Address2, '') as zone, \n"
					+ "    coalesce(email, '') as email, \n"
					+ "    tprecio as tprice, \n"
					+ "    salesrep_id, \n"
					+ "    rutero as route, \n"
					+ "    isdetailednames, \n"
					+ "    lco_taxidtype_id as taxidtype_id, \n"
					+ "    firstname1, \n"
					+ "    firstname2, \n"
					+ "    lastname1, \n"
					+ "    lastname2, \n"
					+ "    coupon \n"
					+ "from HSS_VerClientes \n"
					+ "where ad_client_id = ? \n"
					+ "	and salesrep_id = ? \n";
			
			PreparedStatement pstmt = (PreparedStatement) DB.prepareStatement(sql, trxName);
			pstmt.setInt(1, clientID);
			pstmt.setInt(2, salesRepID);
			ResultSet rs = pstmt.executeQuery();
			
			List<Clientes> clientes = new ArrayList<Clientes>();
			
			while (rs.next()) {
				Clientes cliente = new Clientes();
				cliente.setID(rs.getInt("id"));
				cliente.setValue(rs.getString("value"));
				cliente.setName(rs.getString("name"));
				cliente.setTaxID(rs.getString("taxid"));
				cliente.setAddress(rs.getString("address"));
				cliente.setPhone(rs.getString("phone"));
				cliente.setZone(rs.getString("zone"));
				cliente.setEmail(rs.getString("email"));
				cliente.setTPrice(rs.getString("tprice"));
				cliente.setSalesRep_ID(rs.getString("salesrep_id"));
				cliente.setRoute(rs.getString("route"));
				cliente.setIsDetailedNames(rs.getString("isdetailednames"));
				cliente.setTaxIdType_ID(rs.getInt("taxidtype_id"));
				cliente.setFirstName1(rs.getString("firstname1"));
				cliente.setFirstName2(rs.getString("firstname2"));
				cliente.setLastName1(rs.getString("lastname1"));
				cliente.setLastName2(rs.getString("lastname2"));
				cliente.setCoupon(rs.getString("coupon"));
				
				clientes.add(cliente);
			}
			
			return new Response(true, 0, "", clientes);
		} catch (Exception e) {
			return new Response(false, -1, "Error " + e.getMessage(), "");
		}
	}

	public static Response getVerTipoIdentificacion(int clientID) {
		String trxName = RequestEnv.getCurrentTrxName();
		// Properties ctx = RequestEnv.getCtx();
		
		try {
			String sql = ""
					+ "select lt.lco_taxidtype_id id, \n"
					+ "	lt.name, \n"
					+ "	lt.isdetailednames, \n"
					+ "	ac.hss_cupon coupon 	\n"
					+ "from lco_taxidtype lt \n"
					+ "     JOIN ad_client ac on lt.ad_client_id = ac.ad_client_id \n"
					+ "where lt.ad_client_id = ? \n";
			
			PreparedStatement pstmt = (PreparedStatement) DB.prepareStatement(sql, trxName);
			pstmt.setInt(1, clientID);
			ResultSet rs = pstmt.executeQuery();
			
			List<TipoIdentificacion> tipoIdentificacions = new ArrayList<TipoIdentificacion>();
			
			while (rs.next()) {
				TipoIdentificacion tipoIdentificacion = new TipoIdentificacion();
				tipoIdentificacion.setID(rs.getInt("id"));
				tipoIdentificacion.setName(rs.getString("name"));
				tipoIdentificacion.setIsDetailedNames(rs.getString("isdetailednames").equals("Y"));
				tipoIdentificacion.setCoupon(rs.getString("coupon"));
				
				tipoIdentificacions.add(tipoIdentificacion);
			}
			
			return new Response(true, 0, "", tipoIdentificacions);
		} catch (Exception e) {
			return new Response(false, -1, "Error " + e.getMessage(), "");
		}
	}
}
