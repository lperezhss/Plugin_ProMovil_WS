package com.ingeint.ws.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.compiere.util.DB;

import com.ingeint.ws.base.RequestEnv;
import com.ingeint.ws.presenter.Articulos;
import com.ingeint.ws.presenter.Colores;
import com.ingeint.ws.presenter.Lineas;
import com.ingeint.ws.presenter.Response;
import com.ingeint.ws.presenter.Tallas;

public class SArticulos {

	public static Response getVerArticulos(int clientID) {
		String trxName = RequestEnv.getCurrentTrxName();
		// Properties ctx = RequestEnv.getCtx();
		
		try {
			String sql = ""
					+ "select m_product_id as id, \n"
					+ "    value, \n"
					+ "    name, \n"
					+ "    coalesce(upc, '') as ref, \n"
					+ "    qty stock, \n"
					+ "    producttype as type, \n"
					+ "    help as taxtype, \n"
					+ "    m_product_category_id as category_id, \n"
					+ "    rate as tax, \n"
					+ "    price as perice1, \n"
					+ "    0.00 as perice2, \n"
					+ "    0.00 as perice3, \n"
					+ "    0.00 as perice4, \n"
					+ "    0.00 as perice5, \n"
					+ "    coalesce(UOMName, 'UND') as uom1, \n"
					+ "    coalesce(UOMName, 'UND') as uom2, \n"
					+ "    1.0 as equiuom1, \n"
					+ "    1.0 as equiuom2, \n"
					+ "    iscolorsize, \n"
					+ "    coupon \n"
					+ "from HSS_VerArticulos \n"
					+ "where ad_client_id = ? \n";
			
			PreparedStatement pstmt = (PreparedStatement) DB.prepareStatement(sql, trxName);
			pstmt.setInt(1, clientID);
			ResultSet rs = pstmt.executeQuery();
			
			List<Articulos> articulos = new ArrayList<Articulos>();
			
			while (rs.next()) {
				Articulos articulo = new Articulos();
				articulo.setID(rs.getInt("id"));
				articulo.setValue(rs.getString("value"));
				articulo.setName(rs.getString("name"));
				articulo.setRef(rs.getString("ref"));
				articulo.setStock(rs.getDouble("stock"));
				articulo.setType(rs.getString("type"));
				articulo.setTaxType(rs.getString("taxtype"));
				articulo.setCategory_ID(rs.getString("category_id"));
				articulo.setTax(rs.getDouble("tax"));
				articulo.setPrice1(rs.getDouble("perice1"));
				articulo.setPrice2(rs.getDouble("perice2"));
				articulo.setPrice3(rs.getDouble("perice3"));
				articulo.setPrice4(rs.getDouble("perice4"));
				articulo.setPrice5(rs.getDouble("perice5"));
				articulo.setUOM1(rs.getString("uom1"));
				articulo.setUOM2(rs.getString("uom2"));
				articulo.setEquiUOM1(rs.getDouble("equiuom1"));
				articulo.setEquiUOM2(rs.getDouble("equiuom2"));
				articulo.setIsColorSize(rs.getString("iscolorsize"));
				articulo.setCoupon(rs.getString("coupon"));
				
				articulos.add(articulo);
			}
			
			return new Response(true, 0, "", articulos);
		} catch (Exception e) {
			return new Response(false, -1, "Error " + e.getMessage(), "");
		}
	}

	public static Response getVerLineas(int clientID) {
		String trxName = RequestEnv.getCurrentTrxName();
		// Properties ctx = RequestEnv.getCtx();
		
		try {
			String sql = ""
					+ "select m_product_category_id id, \n"
					+ "	value, \n"
					+ "	name, \n"
					+ " coupon  \n"
					+ "from HSS_VerLineas \n"
					+ "where ad_client_id = ? \n";
			
			PreparedStatement pstmt = (PreparedStatement) DB.prepareStatement(sql, trxName);
			pstmt.setInt(1, clientID);
			ResultSet rs = pstmt.executeQuery();
			
			List<Lineas> lineas = new ArrayList<Lineas>();
			
			while (rs.next()) {
				Lineas linea = new Lineas();
				linea.setID(rs.getInt("id"));
				linea.setValue(rs.getString("value"));
				linea.setName(rs.getString("name"));
				linea.setCoupon(rs.getString("coupon"));
				
				lineas.add(linea);
			}
			
			return new Response(true, 0, "", lineas);
		} catch (Exception e) {
			return new Response(false, -1, "Error " + e.getMessage(), "");
		}
	}

	public static Response getVerColores(int clientID) {
		String trxName = RequestEnv.getCurrentTrxName();
		// Properties ctx = RequestEnv.getCtx();
		
		try {
			String sql = ""
					+ "select hss_productcolor_id id, \n"
					+ "	m_product_id product_id, \n"
					+ "	value, \n"
					+ "	name, \n"
					+ " coupon \n"
					+ "from HSS_VerColores \n"
					+ "where ad_client_id = ? \n";
			
			PreparedStatement pstmt = (PreparedStatement) DB.prepareStatement(sql, trxName);
			pstmt.setInt(1, clientID);
			ResultSet rs = pstmt.executeQuery();
			
			List<Colores> colors = new ArrayList<Colores>();
			
			while (rs.next()) {
				Colores color = new Colores();
				color.setID(rs.getInt("id"));
				color.setProduct_ID(rs.getInt("product_id"));
				color.setValue(rs.getString("value"));
				color.setName(rs.getString("name"));
				color.setCoupon(rs.getString("coupon"));
				
				colors.add(color);
			}
			
			return new Response(true, 0, "", colors);
		} catch (Exception e) {
			return new Response(false, -1, "Error " + e.getMessage(), "");
		}
	}

	public static Response getVerTallas(int clientID) {
		String trxName = RequestEnv.getCurrentTrxName();
		// Properties ctx = RequestEnv.getCtx();
		
		try {
			String sql = ""
					+ "select hss_productsize_id id, \n"
					+ "	m_product_id product_id, \n"
					+ "	value, \n"
					+ "	name, \n"
					+ " coupon  \n"
					+ "from HSS_VerTallas \n"
					+ "where ad_client_id = ? \n";
			
			PreparedStatement pstmt = (PreparedStatement) DB.prepareStatement(sql, trxName);
			pstmt.setInt(1, clientID);
			ResultSet rs = pstmt.executeQuery();
			
			List<Tallas> tallas = new ArrayList<Tallas>();
			
			while (rs.next()) {
				Tallas talla = new Tallas();
				talla.setID(rs.getInt("id"));
				talla.setProduct_ID(rs.getInt("product_id"));
				talla.setValue(rs.getString("value"));
				talla.setName(rs.getString("name"));
				talla.setCoupon(rs.getString("coupon"));
				
				tallas.add(talla);
			}
			
			return new Response(true, 0, "", tallas);
		} catch (Exception e) {
			return new Response(false, -1, "Error " + e.getMessage(), "");
		}
	}
}
